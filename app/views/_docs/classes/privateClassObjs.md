#### Private Variable Object
While creating the ablity to also be able to use object withing the `$$` that are tied to private variables was a royal pain in the ass and consumed my brain one whole afternoon, it was well worth the investemt I think you will agree. 

For demenstration purposes lets take the example of the `hoverGrow` Buttron class part of the `hover package`. Bellow is the basic css markup.
__CSS Output__
```css
.myHoverButton {
  //Defaults..
}
.myHoverButton {
  //Transition options
}
.myHoverButton:hover {
  //..
}
.myHoverButton:active {
  //..
}
.myHoverButton:focus {
  //..
}
```

As you notice all the `state` have the same transition property applied. Now there is three ways to go about creating this Buttron class, first and the worst would be look something like this.
```styl
hoverGrow = ButtronClass('hoverGrow', {
  hover:{
    // Transition props..
  },
  focus:{
    // Transition props..
  }
  active:{
    // Transition props..
  }
})
```

While this works, if you would like to change one of the transistion properties, you would have to change said property in all three locations. This is the wrong way.

The second way to go about this is to create a global variable object in your project, which could be adventagous if you would like multiple classes to share the same properties.
```styl
//Transition Object
_hGrow = {
  transform: scale(1.1)
  options: {
    property: transform
    duration: 0.3s
  },
  globalOptions: {
    property: transform
  }
}

//Class
hoverGrow = CreateButtron({
  hover: _hGrow
  focus: _hGrow
  active: _hGrow
})

//Shared transtion props
someOtherClass = CreateButtron({
  hover: _hGrow
  focus: _hGrow
  active: _hGrow
})
```

As you see now you transition logic is in one central location and can be easlily and convinently be changed. The one major downside with this method is `_hGrow` polutes the global varible scoop, and if you where to share this class with others there would be a _very very_ small change there would be a variable conflict. Nevertheless, it might happen which brings us to magic of private varible objects.

Using private varible object is just like using global objects except they are contained withtin the class itself if that was not self-evedeint. Which brings us to third way to go about creating this `hoverGrow` which is the way its wrote in the hover package.

```styl
hoverGrow = ButtronClass('hoverGrow', {
  $$:{
    //Trans obj vars
    scale: scale(1.1)
    duration: 0.3s
    //Trans obj
    transition: {
      transform: "$$[scale]"
      option: {
        property: transform
        duration: "$$[duration]"
      },
      globalOption: {
        property: transform
      }
    }
  }
  hover: "$$[transition]"
  active: "$$[transition]"
  focus: "$$[transition]"
})

.grower
  hoverGrow()
```

As you can see if you wish you have the ablity to decouple your object varibles from the object itslef to make it more convinent to overerite said varibles. Although, as I said before, and I think its worth repeating the order in which you specify your varibles is important, varibles can only be reffrenced if they are declared above the reffrence point. 

__CSS Output__
```css
.gorwer{
  //defaults
}
.gorwer {
  transition-property: transform;
  transition-duration: 0.3s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.gorwer:hover {
  transition-property: transform;
  transition-duration: 0.3s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  transform: scale(1.1);
}
.gorwer:active {
  //Same as hover
}
.gorwer:focus {
  //Same as hover
}

```

And what if you where to specify a diffrent hover options within the class? Well Buttron will act accordinly and make the proper changes for you.

__Stylus Markup__
```styl
.newGrower
  hoverGrow({
    hover:{
      font-size: 1.1rem
      option: {
        //I can still use class reffrence
        duration: "$$[duration]"
      }
    }
  })
```
__CSS Output__
```css
.newGrower{
  //defaults
}
.newGrower {
  transition-property: all;
  transition-duration: 0.3s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.newGrower:hover {
  transition-property: font-size;
  transition-duration: 0.3s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  font-size: 1.1rem;
}
.newGrower:active {
  transition-property: transform;
  transition-duration: 0.3s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  transform: scale(1.1);
}
.newGrower:focus {
  //Same as active
}
```
