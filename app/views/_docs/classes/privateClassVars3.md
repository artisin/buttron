#### Private Variable Eval Object
Alright, private varibles are cool and all but lets be honest static varibles are just about as useful as Channing Tatum is at acting. And chances are you will want to be able create varables bases on other varables. Well don't you fear becuase `eval` is here. The `eval` object allows you to legerage various built-in stylus function based on other private variables.

__Important,__ the source order of your private varibles matter. A varible can only reffrence another varible if said reffrence varible is declared above. As such:
```styl
//Will Work
example = ButtronClass('example', {
  $$:{
    color: green
    background: "$$[color]"
  }
})

//Will Not Work
example = ButtronClass('example', {
  $$:{
    background: "$$[color]"
    color: green
  }
})
```

##### Eval Colors 
Within `eval` you can leverage the follow stylus built-in color functions.

+ alpha(color, amount)
+ lighten(color, amount)
+ darken(color, amount)
+ desaturate(color, amount)
+ saturate(color, amount)
+ spin(color, amount)
+ tint(color, amount)
+ shade(color, amount)
+ complement(color)
+ invert(color)
+ grayscale(color)


__Stylus Markup__
```styl
evalColor = ButtronClass('evalColor', {
  $$:{
    eval: {
      textColor: "complement($$[color])"
      lbColor: "desaturate($$[color], 70%)"
      tbColor: "invert($$[color])"
      //Reffrence other eval vars
      rbColor: "spin($$[lbColor], 90%)"
      bbColor: "tint($$[tbColor], 60%)"
    }
    //This does not have to be in eval, since the
    //color is already specified
    color: lighten(#C0392B, 20%)
    bWidth: 8px
    bType: solid
  }
  //Props
  font-size: 1.3rem
  font-weight: bold
  background: "$$[color]"
  color: "$$[textColor]"
  border-left: "$$[bWidth]" "$$[bType]" "$$[lbColor]" 
  border-top: "$$[bWidth]" "$$[bType]" "$$[tbColor]" 
  border-right: "$$[bWidth]" "$$[bType]" "$$[rbColor]" 
  border-bottom: "$$[bWidth]" "$$[bType]" "$$[bbColor]"
});

.evalExample
  evalColor()
```

__CSS Output__
```css
.evalExample {
  //Defaults..
  background: #d7584b;
  color: #4bcad7;
  font-size: 1.3rem;
  border-left: 8px solid #a6807c;
  border-top: 8px solid #28a7b4;
  border-right: 8px solid #8da67c;
  border-bottom: 8px solid #a9dbe1;
}

```

