that you have specied w a stylus mixin which when invoked reffrences the spe instance of Buttron that is tied to a  native stylus `mixin` and if you can grasp that you are in for some smooth sailing.

To create a `class` you must first declare a variable _your class name_ followed by the `ButtronClass` constructor. Now this is the most important part, within your `ButtronClass` constructor your first argument will be the __same__ as your __class name__ passed as a string followed by an `object` which will contain all your styles.


> __Sidebar:__ The reason you need to pass the `class` name twice is becuase first we need to create a global stylus mixin variable and then we need a way to reffrance said variable to the global `_Buttron` hash table which the properties are stored in.