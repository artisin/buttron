### Private Class Varibles
Using global stylus variables in your `class` is fine and dany,but lets say your neighboor drops by and askes if you could cook up some _phat material_ classes? Now, because you are a good neighboor you want to package up this class in a nice tidy package, but you also want to give him/her the ablity to __easliy__ customize the colors. This is where private varibles come in handy becuase they give you the ablity to have locolized private class veriables which then can be changed without affecting the global stylus veraible scoop. 

To initilize private varibles you must create a object with the key of `$$` and then list your variables inside. Then you can reffrence those varibles using a the string notation of: `$$[<variable>]`. It is critical you specify these variables as strings and you use bracket notation.

#### Basic Usage
__Stylus Markup__
```styl
privateVars = ButtronClass('privateVars', {
  $$:{
    //List varibles
    bgColor: #1ABC9C
    borderWidth: 5px
    borderColor: #9B59B6
    fz: 1.2rem
  }
  background: "$$[bgColor]"
  font-size: "$$[fz]"
  border: "$$[borderWidth]" solid "$$[borderColor]"
})

.myPrivateButton
  privateVars()
```

__CSS Output__
```css
.myPrivateButton {
  //Defaults..
  background: #1abc9c;
  font-size: 1.2rem;
  border: 5px solid #9b59b6;
}

```




