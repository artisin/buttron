### Imports
There is another class specific helper that opperates in a very simmilar fasion compared to the `$$` object. The idea behind the `import` object is to allow you to create a class to be used as more or less of a skeleton class.framwork in which you can port into diffrent styles into.

For example this is how I created the styles for the bracket package which then allow you to use the following command to change the said styles
```
.brOne
  brackets({
    import:{
      style: "$$[style1]"
    }
  })

.brOne
  brackets({
    $$:{
      bracketColor: green
    }
    import:{
      style: "$$[style1]"
    }
  })

.brTwo
  brackets({
    import:{
      style: "$$[style2]"
    }
  })
```


The main diffrence bettween the `import` and `$$` object is that `import` if for merage object key pairs while the `$$` is used for specifiing pairs. 



__Stylus Markup__
```styl
```

__CSS Output__
```css
```

