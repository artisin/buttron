### About.
Let me tell you a little story about Buttron, there was this one time, when I started to wright a stylus mixin and I forgot to stop. 

Firsts things first, this is my first "real" open-sourse project and I just stared this whole coding thing less than a year and a half ago, hell last year around this time I was just starting to feel somewhat compterbale writing html and css. With that in mind and the fact that my technical wrting abilty is terrible which also describes my writing ablity in general you are in for a real adventure. 

The docs are wordy, riddled with grammer and speeling error, conversational based half the time, its not normal or acidemic, but I made up Buttron and these docs as I went along. And honestly, it you told me that the end project would be as robust and fetuare filled as it currently is I would have had a good laugh. Nevertheless, I keep adding feture after feture till I was satisfied and that sums up the build process. 

PS. I could really use help with the Docs in general and I know there is a word smith reading this right now, so hear my plea and make a pull request.

### What?
In a nutshell Buttron if a tool build to allow you to that allows you to create or import custoized mixins classe specific mixins, yes I did not mis-type there, it allows you to make customizable classes so all your button logic is contained in one neat location. It does all sorts of other cool things but if I did not get your attention with classes well you can't win them all.

Yes, I will repeat that again, this is a package purly written in stylus, ~1800 of delicous, wild, rediculous, stylus.

### Why?
Buttons suck. Well no, `css` sucks or I suck at it or I think I do but thats beside the point. Buttons suck just as much as suits. Buttons much like suits are full of contradictions and illisons. First off, maybe I am just being naive but why the hell do people clutter up their `html` with a bizzionlon classes? Honestly, maybe I am missing something but when I run into `html` element like this: `class = "btn btn-primary btn-lg active someMysteryClass"` I feel like just giving up, like fuck it, I would be better off just being a sharleton. But then again I am a young coder so I have the luxtury of taking my previous statements back when I get some skin in the game.

Anyways, back to the question why? Becuase defined button classes make sense. 

### How?
Honestly, I am not all to sure. I started off creating a [mixin]() for material style buttons when I realized stylus is just an abstraction of javascript. And I procedded to throw the core function of Buttron together in a few days and then I explaimed "fuck this is sorta cool". And the rest is history. 

### Future
To be honest I have had my fair share of `stylusScript` and debugging this whole package using a dumbed down version of console log has took its toll and now the novity and fun is over. So I do not plan any more active development on the stylus code itsself unless of course someone finds a mild-to-sersious bug. However, I do plan to port it over to Js which will be simple as pie compared to the relative challenege this was to make a PostCSS plugin once I get some feedback and input. 

### Production Ready
Yes. Although, if your compiling your stylus at run time for some bizzar and wild reason I would tred with caution. 


### The Docs
I have no idea what I am doing in these Docs, I'm in uncharted stylus teritory with no guide and very litte experiance.

### The ButtronPlace
As I said, I suck at css, and the reason I made the ButtronPlace is so the real wizards could show off their skills and share their creations with the rest of the word. ButtronPlace is build using the all mighty Meteor, which, I will make a selfless plug for, I would love to work with some other cool Meteor people to make some cool Meteor things.
