#### Cool?
Well not really... lets move onto part two and smooth this Buttron out by staggaring the transitions. Fortunalty, this part is ease as pie as well since we can leverage the power of `shorthand`.


__Stylus Markup__
```styl
.multiTransSmooth
  buttron({
    //By applying a transparent border to the defualt state
    //it make the transition silky smooth
    border: solid 5px transparent
    line-height: 50px
    transition:{
      main:{
        option:{
          shorthand:{
            background: 0.5s ease-in 0.5s
            border: 0.5s ease-in
          }
        }
      }
      span:{
        //Indicate to Buttron just to use the set defualts
        applyTo: span
      }
    }
    hover:{
      transition:{
        main:{
          //Since we want to apply this to the parent (.multiTransSmooth) no need for applyTo
          background: transparent
          border: solid 5px #8E44AD
          option: {
            shorthand:{
              background: 0.5s ease-in
              border: 0.5s ease-in 0.5s
            }
          }
        }
        span:{
          applyTo: span
          color: #F1C40F
          background: #8E44AD
          padding:1rem
          option: {
            ease: ease-in-out
          }
        }
        //Per Miss. Johnson Request
        globalOption:{
          duration: 1s
          ease: ease-in
        }
      }
    }
  })
```
__CSS Output__
```css
.multiTransSmooth {
  //Defaults
  border: solid 5px transparent
  line-height: 50px
}
.multiTransSmooth {
  transition: background 0.5s ease-in 0.5s , border 0.5s ease-in;
}
.multiTransSmooth span {
  transition-property: all;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.multiTransSmooth:hover {
  transition: background 0.5s ease-in , border 0.5s ease-in 0.5s;
  background: transparent;
  border: solid 5px #8e44ad;
}
.multiTransSmooth:hover span {
  transition-property: color , background , padding;
  transition-duration: 1s;
  transition-timing-function: ease-in-out;
  transition-delay: 0s;
  color: #f1c40f;
  background: #8e44ad;
  padding: 1rem;
}
```