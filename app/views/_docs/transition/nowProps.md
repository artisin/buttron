### Refract Props
Specifying your trans object and options

+ option
  * property
  * no auto gen of other props

+ auto
  * boolean 

Both - option 1
```
trans:{
  color: green
  shorthand:{
    color: 1s ease-in
  }
  option:{
    delay: 0.25s
  }
}
```


```
trans:{
  color: green
  option:{
    delay: 1s
    shorthand:{
      color: 1s ease-in
    }
  }
}
```




Shorthand only
```
trans:{
  color: green
  shorthand:{
    color: 1s ease-in
  }
}
```

