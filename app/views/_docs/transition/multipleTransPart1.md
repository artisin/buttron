### Multiple Transtions
Just like Miss. Johnson did in math class we are going to build on the previous concepts to make some magic happen. Remeber that consfusing/cyptic sub-child transtion statment I used earlier. Well a here is the [ELI5](http://www.reddit.com/r/explainlikeimfive/): its just multiple transtions within the main transtion that do diffrent things. Becuase in order to turn the previous button into a _bomb-dot-com_ button we are going to need multiple transtions that are staggared.

To make this happen is quite simple, just don't overthink it and find your inner Buttron. Remeber the `transtion` object also takes `children` transitions, so lets use the previous button transtion example add another transtion to it to make two `children` transitions. For the sake of clarity I will do this in two parts, so first lets focus on creating two transtions one applied to `.multiTrans` and the other applied to `.multiTrans span`. 

However, before you could start Miss. Johnson throws you one of her _famous_ curve balls which you think are pointless and stupid yet the jokes on you becuase latter in life you will come to the realiztion those curve balls where not about math at all but rather a metaphor of life. Now thats a _curve ball_. 

Nevertheless, Miss. Johnson tells you she whats all the trasitions to have a duration of 1s. And for `.multiTrans` to use `ease-in` and `.multiTrans span` to use a `ease-in-out`. 

#### Global Option
The `globalOption` object is in essence localized defualts that will overwright the global defaults but not local `option`. 

```styl
globalOption:{
  duration: //..
  ease: //..
  delay: //..
  //Only works if override = true
  property: //..
  override: //..
}
//Alternativly you can also use shorthand
globalOption:{
  shorthand:{
    //..
  }
}
```

If `globalOption` is placed within a `state` it will only affect said `state`. However, if `globalOption` is placed outside of the `state` it will be applied to all `state`'s.
```styl
//Only applied to hover
buttron({
  //..
  hover:{
    //..
    globalOption:{
      duration: 2s
    }
  }
})

//Applied to both hover and active
buttron({
  //..
  hover:{
    //..
  }
  active:{
    //..
  }
  globalOption:{
    duration: 2s
  }
})
```


> __SideBar:__ By defualt you if you specify a `property` within the `globalOption` it will do nothing. However, if you add `override: true` to the `globalOption` it will overwright the default state but not the active state. For example if we where to apply `property: all` and `override: true` on the example bellow, `.multiTrans` and `.multiTrans span` `transition-property` would be set to `all`, although, there active counter parts `.multiTrans:hover` and `.multiTrans:hover span` would not.

So rather then spending our presesous time hardcoding Miss. Johnsons curve ball lets utlize `globalOption`. 

__Stylus Markup__
```styl
.multiTrans
  buttron({
    hover:{
      transition:{
        main:{
          //Since we want to apply this to the parent no need for applyTo
          background: transparent
          border: solid 5px #8E44AD
        }
        span:{
          applyTo: span
          color: #F1C40F
          background: #8E44AD
          padding:1rem
          option: {
            //Will Overwrite global ease
            ease: ease-in-out
          }
        }
      }
      globalOption:{
        duration: 1s
        ease: ease-in
      }
    }
  });
```
__CSS Output__
```css
.multiTrans {
  transition-property: background , border;
  transition-duration: 1s;
  transition-timing-function: ease-in;
  transition-delay: 0s;
}
.multiTrans:hover {
  transition-property: background , border;
  transition-duration: 1s;
  transition-timing-function: ease-in;
  transition-delay: 0s;
  background: transparent;
  border: solid 5px #8e44ad;
}
.multiTrans span {
  transition-property: color , background , padding;
  transition-duration: 1s;
  transition-timing-function: ease-in-out;
  transition-delay: 0s;
}
.multiTrans:hover span {
  transition-property: color , background , padding;
  transition-duration: 1s;
  transition-timing-function: ease-in-out;
  transition-delay: 0s;
  color: #f1c40f;
  background: #8e44ad;
  padding: 1rem;
}
```