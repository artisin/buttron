### The Basics
What would buttons be without transitional effects? Buttron makes adding transitional effects to your buttons as easy as 1-2-3. You will mostly find yourself using transitional effects in combination with buttton states, although, lets first cover the mechanics of the `transition` object. The number one key to remeber with the `transition` object is that is can be used as a stand-alone or used as a parent container object for `children` transitions. Confused, me too, just keep reading on and that statement should become clear.

#### Transition Properties
+ `CSS` properties _(Applied to parent)_
+ `option`
+ `globalOption`
+ `applyTo`
  * `pseudo`
+ `Children` Transitions

#### Basic Use
__Stylus Markup__
```styl
.aTransition
  buttron({
    transition:{
      background: #34495E
      box-shadow: 0px 20px 34px -1px rgba(0,0,0,0.75)
    }
  })
```
__CSS Output__
```css
.aTransition {
  //Defaults
}
.aTransition {
  transition-property: background , box-shadow;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  background: #34495e;
  box-shadow: 0px 15px 25px -1px rgba(0,0,0,0.75);
}
```
_Note: Of course the you do not see this transition happen since it is applied to the directly to `.aTranstion` and therefore it just overwirtes the default properties._