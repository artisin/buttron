#### _Cool_ Example
Alright folks, you know what time it is lets put the puzzle peices together but before we do lets introduce once last concept.



__Stylus Markup__
```styl
phatButton = ButtronClass('phatButton', {
  //Custom Class Vars
  $$:{
    color: alpha(#1ABC9C, 0.75),
    fontSize: 2rem,
    rotateDeg: rotate(180deg),
    borderWidth: 5px
  }
  font-size: "$$[fontSize]"
  color: "$$[color]"
  hover:{
    transform: "$$[rotateDeg]"
  }
  component:{
    span:{
      border-width: "$$[borderWidth]"
    }
  }
})
```



Alright, at first glance don't all this code scare you becuase its lengthy but it is basically just the same snipit of code over and over but its flashy and always tends to impress the on the first date. Furthermore, as demenstrated you are free and incuraged to use stylus veriables within Buttron for fine 

__HTML__
```html
<div class="firstLetter">
  <p>First Letter</p>
</div>
```

