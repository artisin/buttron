### Regex With Child and Type
To help keep your synatx clean any object `key` that statifies the regex of `-child|-type` will be considered an `element`. However, if you specify an `element` key-pair within your object that will supersede the parent key.

__Stylus Markup__
```styl
button
  buttron({
    first-child:{
      height: 400px
    }
    //Will throw erros if not passed as a string
    "nth-last-of-type(2)":{
      font-size: 2rem
    }
    only-child:{
      //Supersedes 
      element: "last-child"
      width: 200px
    }
  })
```

__CSS Output__
```css
button {
  //Defaults
}
button:first-child {
  height: 400px;
}
button:nth-last-of-type(2) {
  font-size: 2rem;
}
button:last-child {
  width: 200px;
}
```

> __Sidebar:__ If you pass `first-child` as your object key stylus will phrase it without any issues but any `element` object key that contains parentheses such as `nth-child(2)` you need to pass it as a string otherwise stylus will phrase it as a `call` and throw an error.