### The Basics
Elements are much like [states](#) in the sense they are appended to their respective parent. Although, unlike states they are treated as sepreate entites from the parent which gives them the ablity to have `states`. The primary pourpose of `elements` is the pacilitate the use of Pseudo-elements and selectors such as `:first-child` which target an `element`. The following are the `elements` that are hardcoded into Buttron which can be declared as the `key` of the object within the parent object just like `states`.
+ `after`
+ `before`
+ `customElement`
+ regex with `-child` and `-type`

`elements` can be used in `classes`, `components`, and within `elements` themselfs and accept `states`, `transitions`, and `animations`.

#### Basic Usage
__Stylus Markup__
```styl
.aElement
  buttron({
    //Element declaration
    <element>:{
      //..
    }
  })
```

__CSS Output__
```css
.aElement:<element> {
  //..
}
```

#### After Example
__HTML Markup__
```html
<div class="afterWhat"></div>
```

__Stylus Markup__
```styl
.afterWhat
  buttron({
    after:{
      content: 'After What?'
    }
  })
```

__CSS Output__
```css
.afterWhat {
  //Defaults
}
.afterWhat:after {
  content: 'After What?';
}
```