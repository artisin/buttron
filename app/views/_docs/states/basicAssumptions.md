#### Basic Assumptions
As you noticed Buttron will automatically generate transition styles for you on `hover`, `active`, and `focus` states for convinence. However, you can override this default by passing `transition: false` within your state. Conversly, for `visited` and `link` by defualt __no__ transition styles will be generated but you can override this defualt by passing `transtion: true`.

__Example__
```styl
.makeItWork
  buttron({
    width: 200px
    link: {
      transition: true
      color: blue
    }
  })
```

```styl
.makeItWork
  buttron({
    width: 200px
    link: {
      color: blue
      option: {
        transition: true
      }
    }
  })
```


~~> __Sidebar:__ If only one `state` is specified Buttron will extract the individual css properites for the parent `transition-property`, although, if more than one `state` is specified the global `transition-property` will default to `all`. Additionaly, if you have a `transition` object within your state it will default to `all` as well, if you with you can always specify indvidual properties within `transition`.~~

__Sidebar:__ If you have multiple states Buttron will try to intelegently complie the correct syntax for you. The following is an example of that.
__Stylus Markup__
```styl
.complieStyle
  buttron({
    width: 200px
    hover: {
      background: green
      option:{
        duration: 0.25s
      }
    }
    active:{
      color: green
      opacity: 0.5
      option: {
        delay: 2s
      }
    }
  })
```
__CSS Output__
```css
.complieStyle {
  width: 200px;
}
.complieStyle {
  transition-property: background, color, opacity;
  transition-duration: 0.25s, 0.5s, 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1), cubic-bezier(0.42, 0, 0.58, 1), cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s, 2s, 2s;
}
.complieStyle:hover {
  background: #008000;
  transition-property: background;
  transition-duration: 0.25s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.complieStyle:active {
  color: #008000;
  opacity: 0.5;
  transition-property: color, opacity;
  transition-duration: 0.5s, 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1), cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 2s, 2s;
}
```