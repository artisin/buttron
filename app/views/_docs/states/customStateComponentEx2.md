#### The Sky Is The Limit
Another fun example for your enjoyment. Keep in mind that if you can dream it Buttron can prbly do it and if not well it will help your prototype your idea.

__HTML Markup__
```html
<div class="doItUp">
  input(type="checkbox" name="radioMe")
  <span>Check Me Out!</span>
</div>
```

__Stylus Markup__
```styl
.doItUp
  buttron({
    style: false
    position: absolute
    component:{
      input:{
        height: 25px
        width: 25px
        z-index: 10
        customState:{
          state: "checked"
          //Span Checked Components
          component:{
            topBorder:{
              element: "span:after"
              selector: "~"
              transition:{
                right: 0%
                bottom: 0%
                option:{
                  shorthand:{
                    right: 0.25s ease-in 
                    bottom: 0.25s ease-in 0.25s
                  }
                }
              }
            }
            bottomBorder:{
              element: "span:before"
              selector: "~"
              transition:{
                left: 0%
                top: calc(0% + 5px)
                border-left: solid 55px alpha(#E74C3C, 0.7)
                option:{
                  shorthand:{
                    left: 0.25s ease-in 0.5s
                    top: 0.25s ease-in 0.75s
                    border-left: 0.25s ease-in 1s
                  }
                }
              }
            }
          }
        }
        component:{
          span:{
            selector: "~"
            after:{
              content: ''
              position: absolute
              top: 0
              left:0
              right: 100%
              bottom: 100%
              border-top: solid 5px alpha(#E74C3C, 0.7)
              border-right: solid 5px alpha(#E74C3C, 0.7)
              transition: {
                option:{
                  shorthand:{
                    right: 0.25s ease-in 0.75s
                    bottom: 0.25s ease-in 0.50s
                  }
                }
              }
            }
            before:{
              content: ''
              position: absolute
              top: calc(100% - 5px)
              left: 100%
              right: 0
              bottom: 0
              border-bottom: solid 5px alpha(#E74C3C, 0.7)
              border-left: solid 5px alpha(#E74C3C, 0.7)
              transition: {
                option:{
                  shorthand:{
                    left: 0.25s ease-in 0.25s
                    top: 0.25s ease-in
                  }
                }
              }
            }
          }
        }
      }
      span:{
        position: absolute
        z-index: -2
        font-size: 1.5rem
        width: 275px
        padding-left: 55px
        left: -15px
        top: -10px
        padding-bottom: 5px
        padding-top: 5px
      }
    }
  })
```
