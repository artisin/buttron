### State Properties

+ Transition object
  * By default if not specified Buttron will assume a transtion
+ Animation object
+ Option Object
  * Transition options
    - duration
    - ease
    - delay
  * applyTo
  * root
  ~~* selector~~
  * Shorthand Object
    - If not specified in parent 
+ Shorthand Object

### Transition and Animation
As I mentioned before by default Buttron assums that every state is a transtion unless spefcified otherwise. For example

```styl
.trainMan
  buttron({
    width: 300px
    hover:{
      //Even though we have set transtion to true here nothing will happen
      //since there is nothing to transtion 
      transtion: true
      anim:{
        name: 'chu-chu'
        timeline:{
          '50':{
            background: teal
          }
        }
      }
    }
  })
```

```css
.trainMan {
  width: 300px;
}
//Timeline
.trainMan:hover {
  animation-name: chu-chu;
  animation-duration: 0.5s;
  animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  animation-iteration-count: 1;
  animation-delay: 0s;
  animation-direction: normal;
  animation-fill-mode: none;
  animation-play-state: running;
}
```


Alternativley if you where to specify a css property and have a animation object it will assume you want to transition those properties

```styl
.trainMan
  buttron({
    width: 300px
    hover:{
      //Assumes to be property you wish to transistion
      color: #eee
      anim:{
        name: 'chu-chu'
        timeline:{
          '50':{
            background: teal
          }
        }
      }
    }
  })
```

```css
.trainMan {
  width: 300px;
}
.trainMan {
  transition-property: color;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
//Timeline
.trainMan:hover {
  color: #eee;
  transition-property: color;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  animation-name: chu-chu;
  animation-duration: 0.5s;
  animation-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  animation-iteration-count: 1;
  animation-delay: 0s;
  animation-direction: normal;
  animation-fill-mode: none;
  animation-play-state: running;
}
```

Neverthless, you like always able to specify a transtion object as well as a animation object with full use of their various notations.

```styl
.trainMan
  buttron({
    width: 300px
    hover:{
      trans:{
        color: #eee
      }
      anim:{
        name: 'chu-chu'
        timeline:{
          '50':{
            background: teal
          }
        }
      }
    }
  })
```

The `css` output is the same as the example above.


#### Option
Within `state`'s you have direct access to the transition [`option`](#) object to modify the transition properites.
__Stylus Markup__
```styl
.transOpts
  buttron({
    hover:{
      //..
      option:{
        //..
      }
    }
  });
```

#### Root
If you use a `state` within a `component` you have can specify if you would like to attach the `state` to the [`root`](#)

#### Apply To
You can specify an `applyTo` target which in will still apply `:hover` to the parent element but apply said style to your `applyTo` targe.

> __Sidebar:__ If you wish to apply `:hover` to `span` rather then to the parenet element you must create a `span` (component)[#]

__Stylus Markup__
```styl
.stateApplyTo
  buttron({
    hover: {
      applyTo: span
      color: #E74C3C
      font-size: 1.5rem
    }
  });
```

__CSS Output__
```css
.stateApplyTo {
  //Defualts
}
.stateApplyTo span {
  transition-property: color , font-size;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.stateApplyTo:hover span {
  transition-property: color , font-size;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  color: #e74c3c;
  font-size: 1.5rem;
}
```
