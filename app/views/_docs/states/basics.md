### The Basics
What would a button be without states? Would it still be a button? I'm no phylosipher so I will leave that up to you to solve. Buttron can handle any state you throw at it out of the box but lefts first go over the common states that are hardcoded into Buttron. Which are as follows:

+ `hover`
+ `active`
+ `focus`
+ `visited`
+ `link`

`States` can be used within `elements`, `classes`, `components`, and work in tandum with `transition` and `animation`. They are more or less jacks of all trades and are designed to be flexible. 

#### Basic Usage
__Stylus Markup__
```styl
.aState
  buttron({
    //state declaration
    <state>:{
      //..
    }
  });
```

__Hover State Example__
```styl
.aHoverState
  buttron({
    style: true
    background: #7F8C8D
    hover:{
      background: #1ABC9C
    }
  });
```
__CSS Output__
```css
.aHoverSate {
  //Defaults
}
.aHoverState {
  transition-property: all;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
}
.aHoverState:hover {
  transition-property: background;
  transition-duration: 0.5s;
  transition-timing-function: cubic-bezier(0.42, 0, 0.58, 1);
  transition-delay: 0s;
  background: #1abc9c;
}
```