### The Animation Basics

####Props
+ name*
+ timeline*
+ option
  * duration
  * ease
  * count
  * delay
  * direction
  * mode
  * state
+ shorthand
  * <name>: <opts>
  * animation-name
  * animation-duration
  * animation-timing-function
  * animation-delay
  * animation-iteration-count
  * animation-direction

To create a anim
```styl
.animTest
  buttron({
    width: 200px
    anim:{
      name: 'basicTest',
      timeline:{
        "25":{
          background: teal
        }
        "50%":{
          background: purple
        }
      }
    }
  })
```



Option A
```styl
.animTest
  buttron({
    style: false
    width: 200px
    anim:{
      multiple: true
      shorthand: {}
      option: {}
      timeline:{
        tmOne:{
          "25":{
            background: teal
          }
          "50%":{
            background: purple
          }
        }
        tmTwo:{
          name: tmTwo
          "25":{
            color: green
          }
          "50%":{
            color: yellow
          }
        }
      }
    }
  })

```


Option B
```styl
.animTest
  buttron({
    style: false
    width: 200px
    anim:{
      tmOne:{
        name: tmOne
        option:{
          count: infinite
        }
        timeline:{
          "25":{
            background: green
          }
          "75":{
            background: teal
          }
        }
      }
      tmTwo:{
        option:{
          duration: 3s
          delay: 1s
        }
        timeline: {
          '25':{
            color: yellow
          }
          '75':{
            color: pink
          }
        }
      }
    }
  })
```