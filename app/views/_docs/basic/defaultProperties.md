### Default Properties
The following list is the default style properties that are applied to your buttron by defualt. If you wish not to have these default styles applied to your Buttron just pass `style: false` to the instance.
- `position: relative`
- `margin: 0rem`
- `border: initial`
- `padding: 0rem`
- `width: 200px`
- `height: 60px`
- `border-radius: 4px`
- `overflow: hidden`
- `cursor: pointer`
- `background: #2196f3`
- `box-shadow: initial`
- `z-index: 0`
- `color: #fff`
- `font-weight: initial`
- `text-align: center`
- `font-size: 1rem`
- `font-family: initial`
- `vertical-align: middle`
- `text-decoration: none`
- `line-height =  height`
  + If specify borders, Buttron will calculate the proper line-height to compenesate. You can always overide this by specifing the line-height yourself.
- `transform: translateZ(0)`
- font-smoothing: `true` (enables the following)
    + `-webkit-backface-visibility: hidden`
    + `backface-visibility: hidden`
    + `-moz-osx-font-smoothing: grayscale`