### Production
There is a `production` mode that you have the option to turn on through setting `production` on the `_Buttron` global varible to true.
```styl
_Buttron['production'] = true
```
_Note: It may be best for you not to turn `production` on depending on your build setup._

#### Removal Of Duplicate Styles 
Buttron will eleminate all dublicate styles and keyframes becuase every instance of Buttron will be complie regardless _(that goes for `classes` as well)_.

#### Stylus Cache is enabled
The easiest way to explain this is to just to show you via code. Alternativly, check out stylus's doc about [cache](https://learnboost.github.io/stylus/docs/bifs.html);

> __Sidebar:__ You can enable `cache` by passing `cache: true` into your instance without enabling production

__Stylus Markup__
```styl
.button1
  buttron()

.button2
  buttron()
```

__CSS Output__ _(without `production`)_
```css
.button1 {
  /* styles */
}

.button2 {
  /* styles */
}
```

__CSS Output__ _(with `production`)_
```css
.button1,
.button2  {
  /* styles */
}
```

And before you say _"thats n64 cool"_ there is a catch, there always is a catch, thats life. The way I have implemented stylus `cache` is through `classes` and if you don't know what I am talking about with regards to `classes` just come back to this. Anyways, stylus will `chache` your Buttron instances via your classes thus your classes must be susinct. Best way to demensrate what I mean is through some code.


__Stylus Markup__
```styl
.button1
  buttron()

.button2
  buttron({
    background: green
  })
```

If production is turned on, `button1` and `button2` will be the same becuase `buttron` is a class at the end of the day. That being said, to take advantage of `cache` your classes mush be the same. Now you can pass `cache: false` and Buttron will not `cache` said instance which gives you some flexablity, although, if you find yourseld using this option offten you are doing something wrong.

#### Should I Turn Production On?
Thats a question for you not me. The reason I say that is some builds take this into consideration such as (cssnano)[https://github.com/ben-eb/cssnano] which does all the above tasks. Additionaly, if you turn on `production` your proformance will take a sinifigant hit, we talking about a serious hit too. 