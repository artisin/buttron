### Custom Properties
There is no limitation on the properties you can pass to Buttron anything goes. However, for more advance properties such as background I would recommend you pass it as a string to ensure Buttron phrases it properly. Nevertheless, as you also see with the `box-shadow` Buttron will attempt and quite possible phares it correctly.
__Adding a Linear-Gradient__
```styl
.aButton
  buttron({
    background: "linear-gradient(135deg, rgba(183,222,237,1) 0%,rgba(113,206,239,1) 50%,rgba(33,180,226,1) 51%,rgba(183,222,237,1) 100%)"
    box-shadow: 0px 3px 8px 1px rgba(0,0,0,0.75)
  })
```
