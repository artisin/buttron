### Changing Properties
Changing properties is as simple as passing a property to buttron in a `object` formate. It will either overwrite the defualt or add the new property.
__Changing the background__
```styl
.aButton
  buttron({
    background: green
  })
```
